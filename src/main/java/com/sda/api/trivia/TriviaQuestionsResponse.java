package com.sda.api.trivia;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TriviaQuestionsResponse {
    @JsonProperty("response_code")
    private int responseCode;

    private List<TriviaQuestion> results;
}
