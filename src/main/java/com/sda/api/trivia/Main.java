package com.sda.api.trivia;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            List<Category> categories = getCategoriesFromAPI();

            System.out.println("Dostępne kategorie: ");
            for (Category category : categories) {
                System.out.println(category.getId() + " -> " + category.getName());
            }

            System.out.println("Wybierz numerek:");
            int identyfikator = scanner.nextInt();

            // TODO: pozostałe parametry/pytania

            List<TriviaQuestion> questions = getQuestionsFromAPI(identyfikator);

            System.out.println(questions);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static List<TriviaQuestion> getQuestionsFromAPI(int identyfikator) throws IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();

        String response = sendRequestToAPI("https://opentdb.com/api.php?amount=10&category=" + identyfikator);

        TriviaQuestionsResponse questionsResponse = objectMapper.readValue(response, TriviaQuestionsResponse.class);

        return questionsResponse.getResults();
    }

    private static String sendRequestToAPI(String url) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        String responseString = response.body();

        return responseString;
    }

    private static List<Category> getCategoriesFromAPI() throws IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();

        String responseString = sendRequestToAPI("https://opentdb.com/api_category.php");

        CategoriesResponse categoriesResponse = objectMapper.readValue(responseString, CategoriesResponse.class);

        return categoriesResponse.getTriviaCategories();
    }
}
